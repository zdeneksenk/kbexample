package cz.senk.kbexample.wsclient;

import java.math.BigDecimal;

/**
 * Klient pro komunikaci s webovou službou
 * 
 * @author senk
 *
 */
public interface WebServiceClient {

	BigDecimal convertToUsd(BigDecimal czk);

	BigDecimal convertToEur(BigDecimal czk);
}
