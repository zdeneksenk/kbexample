package cz.senk.kbexample.wsclient;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.google.common.base.Preconditions;

import cz.senk.kbexample.webservices.EnumCurrencies;
import cz.senk.kbexample.webservices.conversionservice.ConversionRequest;
import cz.senk.kbexample.webservices.conversionservice.ConversionResponse;

/**
 * 
 * @author senk
 *
 */
@Component
public class WebServiceClientImpl implements WebServiceClient {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private WebServiceTemplate webServiceTemplate;
	

	@Override
	public BigDecimal convertToUsd(BigDecimal czk) {
		return callConversionToCurrencyUnit(czk, EnumCurrencies.USD);
	}

	@Override
	public BigDecimal convertToEur(BigDecimal czk) {
		return callConversionToCurrencyUnit(czk, EnumCurrencies.EUR);
	}

	
	private BigDecimal callConversionToCurrencyUnit(BigDecimal czk, EnumCurrencies target) {
		Preconditions.checkNotNull(czk);
		Preconditions.checkNotNull(target);
		
		logger.debug("Zahajuji volani konverzni sluzby pro castku '{}' v mene 'CZK'", czk);
		
		ConversionRequest conversionRequest = new ConversionRequest();
		conversionRequest.setAmountValueInCzk(czk);
		conversionRequest.setTargetCurrency(target);
		
		ConversionResponse response = (ConversionResponse) webServiceTemplate.marshalSendAndReceive(conversionRequest);
		BigDecimal usd = response.getConversion().getAmountValue();

		logger.debug("Konverzni sluzba vratila vysledek '{}' v mene '{}'", usd, target);
		return usd;
	}
}
