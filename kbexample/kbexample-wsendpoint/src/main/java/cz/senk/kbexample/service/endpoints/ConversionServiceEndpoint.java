package cz.senk.kbexample.service.endpoints;

import java.math.BigDecimal;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import cz.senk.kbexample.service.ConversionService;
import cz.senk.kbexample.webservices.Conversion;
import cz.senk.kbexample.webservices.conversionservice.ConversionRequest;
import cz.senk.kbexample.webservices.conversionservice.ConversionResponse;

/**
 * Endpoint služby
 * 
 * @author senk
 *
 */
@Endpoint
public class ConversionServiceEndpoint {
	private static final String TARGET_NAMESPACE = "http://cz/senk/kbexample/webservices/conversionservice";

	@Autowired
	private ConversionService conversionService; 

	/**
	 * Zdrojová měna je vždy CZK
	 */
	private static final CurrencyUnit SOURCE_CURRENCY = Monetary.getCurrency("CZK");

	/**
	 * 
	 * @param request
	 * @return
	 */
	@PayloadRoot(localPart = "ConversionRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ConversionResponse getConversion(@RequestPayload ConversionRequest request) {
		// prevedeni do API moneta knihovny
		MonetaryAmount sourceAmount = Money.of(request.getAmountValueInCzk(), SOURCE_CURRENCY);
		CurrencyUnit targetCurrency = Monetary.getCurrency(request.getTargetCurrency().toString());
		
		// zavolani konverze
		MonetaryAmount targetAmount = conversionService.convert(sourceAmount, targetCurrency);

		// a sestaveni odpovedi
		ConversionResponse response = new ConversionResponse();
		Conversion conversion = new Conversion();
		conversion.setAmountCurrency(request.getTargetCurrency());
		conversion.setAmountValue(new BigDecimal(targetAmount.getNumber().toString()));
		response.setConversion(conversion);
		
		return response;
	}
	
	public void setConversionService(ConversionService conversionService) {
		this.conversionService = conversionService;
	}
}