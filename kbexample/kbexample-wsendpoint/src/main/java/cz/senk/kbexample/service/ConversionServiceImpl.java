package cz.senk.kbexample.service;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

import org.springframework.stereotype.Service;

/**
 * Implementace pomocí moneta knihovny
 * 
 * @author senk
 *
 */
@Service
public class ConversionServiceImpl implements ConversionService {

	@Override
	public MonetaryAmount convert(MonetaryAmount source, CurrencyUnit targetCurrency) {
		
		CurrencyConversion dollarConversion = MonetaryConversions.getConversion(targetCurrency);
		
		return source.with(dollarConversion);
	}

}
