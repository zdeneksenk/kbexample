package cz.senk.kbexample.service;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

/**
 * Služba pro konverzi kurun na cílovou měnu
 * 
 * @author senk
 *
 */
public interface ConversionService {

	public MonetaryAmount convert(MonetaryAmount source, CurrencyUnit targetCurrency);
}
