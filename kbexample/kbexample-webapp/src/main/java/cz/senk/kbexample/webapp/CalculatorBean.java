package cz.senk.kbexample.webapp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cz.senk.example.domain.Cost;
import cz.senk.example.service.CalculatorService;
import cz.senk.example.util.CalendarUtil;

@Component
@ManagedBean
@SessionScoped
public class CalculatorBean {

	private Date dateOfBirth;
	private BigDecimal annualSalary;

	@Autowired
	private CalculatorService calculatorService;

	public List<Cost> getCosts() {
		return calculatorService.getCosts();
	}
	
	public void addPerson() {
		calculatorService.computeCost(this.dateOfBirth, this.annualSalary);
		
		this.annualSalary = null;
		this.dateOfBirth = null;
	}


	public String showGreeting() {
		return "Hello" + " " + null + " " + null + "!";
	}


	
	public Date getMindate() {
		LocalDate now = CalendarUtil.toLocalDate(new Date()).minusYears(60);
		return CalendarUtil.toDate(now);
	}

	public Date getMaxdate() {
		return new Date();
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	
	public BigDecimal getAnnualSalary() {
		return annualSalary;
	}

	public void setAnnualSalary(BigDecimal annualSalary) {
		this.annualSalary = annualSalary;
	}
}
