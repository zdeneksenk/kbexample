package cz.senk.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.senk.example.domain.Cost;


/**
 * 
 * @author senk
 *
 */
public interface CostRepository extends JpaRepository<Cost, Long>{

}
