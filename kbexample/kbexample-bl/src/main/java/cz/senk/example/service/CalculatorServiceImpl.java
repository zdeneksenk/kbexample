package cz.senk.example.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.senk.example.dao.CostRepository;
import cz.senk.example.domain.Cost;
import cz.senk.example.exception.ApplicationException;
import cz.senk.example.util.CostUtil;
import cz.senk.kbexample.wsclient.WebServiceClient;


@Service
public class CalculatorServiceImpl implements CalculatorService {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CostRepository costRepository;

	@Autowired
	private WebServiceClient webServiceClient;


	@Override
	public Cost computeCost(Date birthDate, BigDecimal annualSalary) {
		try {			
			logger.info("Info message");
			logger.debug("Info message");
			
			Cost cost = new Cost();

			cost.setAnnualSalaryCzk(annualSalary);
			cost.setBirthDate(birthDate);
			cost.setStaffCostsCzk(CostUtil.computeCostForPeron(birthDate, annualSalary));
			cost.setStaffCostsEur(webServiceClient.convertToEur(cost.getStaffCostsCzk()));
			cost.setStaffCostsUsd(webServiceClient.convertToUsd(cost.getStaffCostsCzk()));
			cost.setComputationDate(new Date());

			Cost persisted = costRepository.saveAndFlush(cost);

			return persisted;
		} catch (Exception e) {
			logger.error("Doslo k chybe pri vypoctu nakladu", e);
			throw new ApplicationException("Doslo k chybe pri vypoctu nakladu", e);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cost> getCosts() {
		return costRepository.findAll();
	}

}
