package cz.senk.example.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import cz.senk.example.domain.Cost;

/**
 * Pomocná třída pro výpočty nákladů
 * 
 * @author senk
 *
 */
public class CostUtil {

	public static BigDecimal computeCostForPeron(Date dateOfBirth, BigDecimal annualSalary) {
		int inputAge = computeInputAge(dateOfBirth);
		BigDecimal monthlySalary = computeMonthlySalary(annualSalary);
		
		BigDecimal staffCost = monthlySalary.multiply(new BigDecimal(60 - inputAge));
		return staffCost;
		
	}
	
	private static BigDecimal computeMonthlySalary(BigDecimal annualSalary) {
		return annualSalary.divide(new BigDecimal(12), Cost.MONETARY_SCALE, Cost.ROUNDING);
	}
	
	private static int computeInputAge(Date dateOfBirth) {
		LocalDate dateOfBirthLd = CalendarUtil.toLocalDate(dateOfBirth);
		LocalDate nowLd = CalendarUtil.toLocalDate(new Date());
		
		return (nowLd.getYear() - dateOfBirthLd.getYear());
	}
	
}
