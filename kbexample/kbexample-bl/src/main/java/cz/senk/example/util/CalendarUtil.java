package cz.senk.example.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Pomocné metody pro práci s datumy
 * 
 * @author senk
 *
 */
public class CalendarUtil {
	
	public static final ZoneId DEFAULT_ZONE = ZoneId.systemDefault();
	
	
	
	public static LocalDate toLocalDate(Date date) {
		return date.toInstant().atZone(DEFAULT_ZONE).toLocalDate();
	}
	
	public static Date toDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay(DEFAULT_ZONE).toInstant());
	}
	
	
	
}
