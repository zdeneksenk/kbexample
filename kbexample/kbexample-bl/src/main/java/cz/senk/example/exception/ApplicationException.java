/**
 * 
 */
package cz.senk.example.exception;

import java.io.Serializable;

/**
 * Bázová třída pro výjimky business vrstvy
 * 
 * @author senk
 *
 */
public class ApplicationException extends RuntimeException implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public ApplicationException() {
		super();
	}

	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

}
