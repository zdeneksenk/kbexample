package cz.senk.example.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cz.senk.example.domain.Cost;
import cz.senk.example.exception.ApplicationException;

/**
 * Service pro výpočet a získání nákladů
 * 
 * @author senk
 *
 */
public interface CalculatorService {

	/**
	 * Provede výpočet nákladu
	 * 
	 * @param birthDate datum narození 
	 * @param annualSalary roční plat
	 * @return Náklad
	 * @throws ApplicationException pokud dojde při výpočtu k chybě
	 */
	Cost computeCost(Date birthDate, BigDecimal annualSalary) throws ApplicationException;
	
	List<Cost> getCosts();
}
