package cz.senk.example.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 * @author senk
 *
 */
@Entity
@Table(name = Cost.TABLE)
public class Cost implements Serializable {

	@Transient
	private static final long serialVersionUID = 5L;

	@Transient
	public static final RoundingMode ROUNDING = RoundingMode.HALF_EVEN;

	/**
	 * Celkový počet číslic v čísle, tj. před i za desetinnou čárkou (tečkou).
	 */
	@Transient
	public static final int MONETARY_PRECISION = 19;
		
	/**
	 * Kolik číslic má tvořit desetinnou část.
	 */
	@Transient
	public static final int MONETARY_SCALE = 4;
	


	@Transient
	public static final String TABLE = "naklady";
	
	@Id
	@GeneratedValue
	private Long id;
		
	@Column(name = "datum_narozeni")
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	@Column(name = "datum_vypoctu")
	@Temporal(TemporalType.DATE)
	private Date computationDate;
	
	@Column(name = "rocni_plat", scale = MONETARY_SCALE, precision = MONETARY_PRECISION)
	private BigDecimal annualSalaryCzk;
	
	@Column(name = "mesicni_naklad_czk", scale = MONETARY_SCALE, precision = MONETARY_PRECISION)
	private BigDecimal staffCostsCzk;
	
	@Column(name = "mesicni_naklad_eur", scale = MONETARY_SCALE, precision = MONETARY_PRECISION)
	private BigDecimal staffCostsEur;

	@Column(name = "mesicni_naklad_usd", scale = MONETARY_SCALE, precision = MONETARY_PRECISION)
	private BigDecimal staffCostsUsd;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public BigDecimal getAnnualSalaryCzk() {
		return annualSalaryCzk;
	}

	public void setAnnualSalaryCzk(BigDecimal annualSalaryCzk) {
		this.annualSalaryCzk = annualSalaryCzk;
	}

	public BigDecimal getStaffCostsCzk() {
		return staffCostsCzk;
	}

	public void setStaffCostsCzk(BigDecimal staffCostsCzk) {
		this.staffCostsCzk = staffCostsCzk;
	}

	public BigDecimal getStaffCostsEur() {
		return staffCostsEur;
	}

	public void setStaffCostsEur(BigDecimal staffCostsEur) {
		this.staffCostsEur = staffCostsEur;
	}

	public BigDecimal getStaffCostsUsd() {
		return staffCostsUsd;
	}

	public void setStaffCostsUsd(BigDecimal staffCostsUsd) {
		this.staffCostsUsd = staffCostsUsd;
	}

	public Date getComputationDate() {
		return computationDate;
	}

	public void setComputationDate(Date computationDate) {
		this.computationDate = computationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cost other = (Cost) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
